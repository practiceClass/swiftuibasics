//
//  FacebookLayout.swift
//  swiftUIBasics
//
//  Created by shreejwal giri on 3/7/20.
//  Copyright © 2020 shreejwal giri. All rights reserved.
//

import SwiftUI

struct TrendingData: Identifiable {
    var id: Int
    let description, image: String
}

struct PostData: Identifiable {
    var id: Int
    let name, title, description, postedDate, image: String
}

struct FacebookLayout: View {
    
    let trendingList: [TrendingData] = [
        .init(id: 0, description: "this is the trending list and hope it scrolls horizantal", image: "one"),
        .init(id: 1, description: "this is the trending list and hope it scrolls horizantal", image: "two"),
        .init(id: 2, description: "this is the trending list and hope it scrolls horizantal", image: "one"),
        .init(id: 3, description: "this is the trending list and hope it scrolls horizantal", image: "two"),
        .init(id: 4, description: "this is the trending list and hope it scrolls horizantal", image: "two"),
        .init(id: 5, description: "this is the trending list and hope it scrolls horizantal", image: "two"),
        .init(id: 6, description: "this is the trending list and hope it scrolls horizantal", image: "two")
    ]
    
    let postList: [PostData] =  [
        .init(id: 0, name: "ram bahadur", title: "dogs", description: "this is a dog and this has four legs", postedDate: "posted 9h Ago", image: "one"),
        .init(id: 1, name: "krishna dotel", title: "cats", description: "this is a dog and this has four legs", postedDate: "posted 9h Ago", image: "two"),
        .init(id: 2, name: "bidya kuwar", title: "puppy", description: "this is a dog and this has four legs", postedDate: "posted 9h Ago", image: "three"),
        .init(id: 3, name: "pappu dongol", title: "hippo", description: "this is a dog and this has four legs", postedDate: "posted 9h Ago", image: "one")
        
    ]
    
    var body: some View {
        NavigationView {
            List {
                TrendingLayout(trendingList: trendingList)
                PostLayout(postList: postList)
            }
            .navigationBarTitle(Text("Groups"))
        }
    }
}

struct GroupDetailView : View {
    var body: some View {
        Text("group View")
    }
}

struct TrendingLayout: View {
    let trendingList: [TrendingData]
    var body: some View {
        ScrollView (.horizontal) {
            HStack {
                ForEach (trendingList) { (data) in
                    NavigationLink (destination: GroupDetailView()) {
                        VStack (alignment: .leading) {
                            Image(data.image)
                                .renderingMode(.original)
                                .resizable()
                                .frame(width: 80, height: 90)
                                .aspectRatio(contentMode: .fit)
                                .clipped()
                            
                            Text(data.description)
                                .font(.footnote)
                                .multilineTextAlignment(.leading)
                                .accentColor(.primary)
                        }
                        .frame(width: 90)
                    }
                    
                }
            }.padding(.leading, 0).padding(.trailing, -20)
        }
    }
}

struct PostLayout: View {
    let postList: [PostData]
    var body: some View {
        ForEach (postList) { (data) in
            VStack (alignment: .leading) {
                Group {
                    HStack {
                        Image(data.image)
                            .resizable()
                            .frame(width: 40, height: 40)
                            .aspectRatio(contentMode: .fill)
                            .clipShape(Circle())
                        
                        VStack (alignment: .leading) {
                            Text(data.name).font(.headline)
                            Text(data.postedDate).font(.footnote)
                        }
                    }
                    Text(data.description)
                }.padding(EdgeInsets(top: 0, leading: 10, bottom: 0, trailing: 10))
                Group {
                    Image(data.image)
                        .resizable()
                        .aspectRatio(contentMode: .fit)
                }
            }.padding(.leading, -20).padding(.trailing, -20)
            
        }
    }
}

struct FacebookLayout_Previews: PreviewProvider {
    static var previews: some View {
        FacebookLayout()
    }
}
