//
//  ContentView.swift
//  swiftUIBasics
//
//  Created by shreejwal giri on 3/7/20.
//  Copyright © 2020 shreejwal giri. All rights reserved.
//

import SwiftUI

struct Users: Identifiable {
    var id: Int
    let username, message: String, imageName: String
}

struct ContentView: View {
    let users: [Users] = [
        .init(id: 0, username: "Shreejwal Giri", message: "I work as An IOS Developer in a Fintech company", imageName: "one"),
        .init(id: 1, username: "Ram Bahadur", message: "I don't know where he works at", imageName: "two"),
        .init(id: 2, username: "Ram Bahadur Krishna", message: "I don't know where he works at", imageName: "three")
    ]
    var body: some View {
        NavigationView {
            List {
                ForEach(users) { user in
                    UserRow(user: user)
                }
            }.navigationBarTitle(Text("Dynamic List"))
        }
    }
}

struct UserRow: View {
    let user: Users
    var body: some View {
        HStack {
            Image(user.imageName)
                .resizable()
                .clipShape(Circle())
                .aspectRatio(contentMode: .fill)
                .overlay(Circle().stroke(Color.gray, lineWidth: 3))
                .frame(width: 80, height: 80)
            VStack (alignment: .leading) {
                Text(String(user.username)).font(.headline)
                Text(String(user.message)).font(.footnote)
            }.padding(.leading, 10)
        }.padding(EdgeInsets(top: 10, leading: 0, bottom: 10, trailing: 0))
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
